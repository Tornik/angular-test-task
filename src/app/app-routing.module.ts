import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'home', loadChildren: () => import('./modules/landing/landing.module').then(m => m.LandingModule)},
  { path: 'subscription',  loadChildren: () => import('./modules/subscription/subscription.module').then(m => m.SubscriptionModule) },
  { path: '**', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
