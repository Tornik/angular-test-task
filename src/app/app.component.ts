import { Component } from '@angular/core';
import * as AOS from 'aos';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor() {
    AOS.init({
      offset: 200,
      delay: 50,
      duration: 500,
      once: true,

    });
  }
}
