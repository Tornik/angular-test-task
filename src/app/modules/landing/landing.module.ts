import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingRoutingModule } from './landing-routing.module';
import { WelcomeTitleComponent, HomeComponent, SubscribeComponent,
  GetStartedComponent, PowerfullWrapperComponent, WelcomeCardsWrapperComponent } from './components/';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [
    WelcomeTitleComponent,
    HomeComponent,
    WelcomeCardsWrapperComponent,
    PowerfullWrapperComponent,
    GetStartedComponent,
    SubscribeComponent,

  ],
  imports: [
    CommonModule,
    LandingRoutingModule,
    SharedModule
  ]
})
export class LandingModule { }
