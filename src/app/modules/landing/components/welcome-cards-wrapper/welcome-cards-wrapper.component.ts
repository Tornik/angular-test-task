import { Component } from '@angular/core';
import {transition, trigger, useAnimation} from '@angular/animations';
import {slideInDown} from 'ng-animate';

@Component({
  selector: 'app-welcome-cards-wrapper',
  templateUrl: './welcome-cards-wrapper.component.html',
  styleUrls: ['./welcome-cards-wrapper.component.scss'],
  animations: [
    trigger('slideInDown', [transition('* => *', useAnimation(slideInDown))])
  ]
})
export class WelcomeCardsWrapperComponent {
  slideInDown: any;
  constructor() { }
}
