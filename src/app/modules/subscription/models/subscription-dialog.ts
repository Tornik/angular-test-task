import {Plan} from './plan';
import {ProductTypeEnum} from '../enum/product';

export interface SubscriptionDialog {
  name: ProductTypeEnum;
  plan: Plan;
}
