import {PaymentTypeEnum, CurrencyEnum} from '../enum/';

export interface Plan {
  id: string;
  paymentInterval?: PaymentTypeEnum;
  interval?: PaymentTypeEnum;
  priceCents: number;
  currency: CurrencyEnum;
}
