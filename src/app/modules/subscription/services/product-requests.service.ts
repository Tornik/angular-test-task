import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import { Products } from '../models';

@Injectable()
export class ProductRequestsService {

  constructor(private http: HttpClient) { }

  public getAllProducts(): Observable<Products> {
    return this.http.get<Products>(`${environment.apiUrl}products`);
  }
}
