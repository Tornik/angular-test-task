import {Component, Input, OnInit} from '@angular/core';
import {ProductRequestsService} from '../../services/';
import {Product, SubscriptionDialog} from '../../models/';
import {PaymentTypeEnum} from '../../enum/';
import {finalize, takeUntil} from 'rxjs/operators';
import {SubscriptionsComponent, SubscriptionDialogComponent} from '../../../shared/components/';
import { MatDialog } from '@angular/material/dialog';
import {style, transition, trigger, useAnimation, state, animate} from '@angular/animations';
import {fadeIn, fadeOut} from 'ng-animate';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  animations: [
    trigger('fadeOut', [transition('* => *', useAnimation(fadeOut))]),
    trigger('fadeIn', [
      transition('* => *', useAnimation(fadeIn)),
      ]),
    ]
})
export class ProductComponent extends SubscriptionsComponent implements OnInit {
  fadeIn: any;
  fadeOut: any;

  @Input() set paymentType(value: PaymentTypeEnum) {

    this.selectedPaymentType = value;
    this.fadeIn = true;
  }

  public loading = true;
  public products: Product[] = [];
  public selectedPaymentType: PaymentTypeEnum;
  constructor(private productRequestsService: ProductRequestsService, public dialog: MatDialog) {
    super();
  }

  ngOnInit(): void {
    this.getAllProducts();
  }

  openDialog(element: Product) {
    this.dialog.open(SubscriptionDialogComponent, {
      data: {
        name: element.name,
        plan: element.plans.find(item => item.interval === this.selectedPaymentType || item.paymentInterval === this.selectedPaymentType),
      } as SubscriptionDialog,
    });
  }

  private getAllProducts() {
    this.productRequestsService.getAllProducts()
      .pipe(
        takeUntil(this.ngUnsubscribe),
        finalize(() => {
          this.loading = false;
        })
      )
      .subscribe(
      res => {
        this.products = res?.products;
      }
    );
  }
}
