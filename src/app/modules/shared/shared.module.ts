import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { FooterComponent, LayoutComponent, NavigationComponent,
  FooterNavigationComponent, SubscriptionsComponent, LoaderComponent, SubscriptionDialogComponent } from './components/';
import {RouterModule} from '@angular/router';
import { SymbolBetweenNumbersPipe } from './pipes/symbol-between-numbers.pipe';
import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  declarations: [
    NavigationComponent,
    FooterComponent,
    LayoutComponent,
    FooterNavigationComponent,
    SubscriptionsComponent,
    LoaderComponent,
    SymbolBetweenNumbersPipe,
    SubscriptionDialogComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    RouterModule,
    MatButtonModule,
    MatDialogModule
  ],
  exports: [
    LayoutComponent,
    MatButtonModule,
    SubscriptionsComponent,
    LoaderComponent,
    SymbolBetweenNumbersPipe,
    SubscriptionDialogComponent,
    MatDialogModule,
  ],
  entryComponents: [
    SubscriptionDialogComponent
  ],
  providers: [
  ],
})
export class SharedModule { }
