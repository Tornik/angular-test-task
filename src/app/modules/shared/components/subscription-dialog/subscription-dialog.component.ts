import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {SubscriptionDialog} from '../../../subscription/models/subscription-dialog';
import {Plan} from '../../../subscription/models/plan';

@Component({
  selector: 'app-subscription-dialog',
  templateUrl: './subscription-dialog.component.html',
  styleUrls: ['./subscription-dialog.component.scss']
})
export class SubscriptionDialogComponent {
  public dialogData: SubscriptionDialog;
  public plan: Plan;
  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: SubscriptionDialog) {
    this.dialogData = data;
    this.plan = data.plan;
  }

}
