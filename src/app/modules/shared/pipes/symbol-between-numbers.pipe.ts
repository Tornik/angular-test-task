import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'symbolBetweenNumbers'
})
export class SymbolBetweenNumbersPipe implements PipeTransform {

  transform(value: number, symbol: string): string {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, symbol);
  }

}
